import pandas
from currency_converter import CurrencyConverter


def add_column(df, col_name):
    unique_items = data[col_name].unique().tolist()
    for item in unique_items:
        print(item)
        s = col_name + "_is_" + str(item)
        df[s] = (data[col_name] == item).astype(int)

    return df


def convert_amount(df):
    c = CurrencyConverter()
    converted = []
    for i in range(len(df.index)):
        amount = df["amount"].values[i]
        currency = df["currencycode"].values[i]
        converted.append(int(c.convert(amount, currency, "USD")))
    df["amountdollar"] = converted
    return df


def temporal(data, df):
    malicious_bin = []
    malicious_mail_id = []
    malicious_ip_id = []
    malicious_card_id = []
    dates = data["creationdate"].values
    bins_used = data["bin"].values
    mail_ids_used = data["mail_id"].values
    ip_ids_used = data["ip_id"].values
    card_ids_used = data["card_id"].values
    for i in range(len(data.index)):
        date = dates[i]
        bin_used = bins_used[i]
        mail_id_used = mail_ids_used[i]
        ip_id_used = ip_ids_used[i]
        card_id_used = card_ids_used[i]
        rows = df.loc[(df["bin"] == bin_used) & (df["bookingdate"] < date)]
        malicious_bin.append(int(len(rows.index) > 0))
        rows = df.loc[(df["mail_id"] == mail_id_used) & (df["bookingdate"] < date)]
        malicious_mail_id.append(int(len(rows.index) > 0))
        rows = df.loc[(df["ip_id"] == ip_id_used) & (df["bookingdate"] < date)]
        malicious_ip_id.append(int(len(rows.index) > 0))
        rows = df.loc[(df["card_id"] == card_id_used) & (df["bookingdate"] < date)]
        malicious_card_id.append(int(len(rows.index) > 0))
        if i % 1000 == 0: print(i)
    return malicious_bin, malicious_mail_id, malicious_ip_id, malicious_card_id


file_path = "data/data_for_student_case.csv"
data = pandas.read_csv(file_path, header=0)
# Take out refused data
data = data[data.simple_journal != "Refused"]
# Add label
data["label"] = (data["simple_journal"] == "Chargeback").astype(int)

# issuercountrycode, for each country add a column in the form of issuercountrycode_is_CODE
data = add_column(data, "issuercountrycode")
# txvariantcode, for each card type add a column in the form of txvariantcode_is_CODE
data = add_column(data, "txvariantcode")
# amount, for each amount that is not in dollars, convert it to dollars
data = convert_amount(data)
# currencycode, for each currency add a column in th form of currencycode_is_CODE
data = add_column(data, "currencycode")
# shoppercountrycode, for each country add a column in the form of shoppercountrycode_is_CODE
data = add_column(data, "shoppercountrycode")
# shopperinteraction, for each type of interaction add a column in the form of shopperinteraction_is_CODE
data = add_column(data, "shopperinteraction")
# cardverificationcodesupplied, for each response add a column in the form of cardverificationcodesupplied_is_SUPPLIEd
data = add_column(data, "cardverificationcodesupplied")
# accountcode, for each accountcode add a column in the form of accountcode_is_CODE
data = add_column(data, "accountcode")

# issuercountrycode and shoppercountrycode, check whether equal
data["issuerequalsshopper"] = (data["issuercountrycode"] == data["shoppercountrycode"]).astype(int)

# Temporal features, may only check using bookingdate after creationdate
# Is bin/mail/ip/card used in previous malicious activity?
# Note, this can probably done faster using some pandas magic, just not sure how
data["bin_malicious"], data["mail_id_malicious"], data["ip_id_malicious"], data["card_id_malicious"] = \
    temporal(data, data[data.simple_journal == "Chargeback"])

data = data.drop(
    ["bin", "txid", "issuercountrycode", "txvariantcode", "currencycode", "shoppercountrycode", "shopperinteraction",
     "simple_journal", "cardverificationcodesupplied", "creationdate", "bookingdate", "accountcode", "mail_id", "ip_id",
     "card_id"], axis=1)

# Sort so the chargebacks are in the top rows
data = data.sort_values("label", ascending=False)

save_path = "data/processed.csv"
data.to_csv(save_path, sep=',', index=False)
