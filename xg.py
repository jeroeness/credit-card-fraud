import sklearn
from sklearn.metrics import confusion_matrix
import pandas
from sklearn.model_selection import KFold
from sklearn.metrics import precision_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import recall_score
from sklearn.metrics import roc_auc_score
from sklearn.tree import DecisionTreeClassifier
import os
from sklearn.model_selection import cross_val_score
from sklearn import tree
from imblearn.over_sampling import SMOTE
from sklearn.datasets import make_classification
from sklearn.metrics import f1_score, confusion_matrix
import xgboost as xgb


def get_data():
    return pandas.read_csv("data/processed.csv")


def split_test_train(data, train_size):
    return sklearn.model_selection.train_test_split(data, train_size=train_size)


def xgboost(train, train_labels, test, test_labels):
    # XGBoost setup
    weight = (train_labels.size - train_labels.sum()) / train_labels.sum()
    params = {"objective": "binary:hinge",
              "pos_weight": weight}
    # Create DMatrix required by XGBoost, train model
    xg_train = xgb.DMatrix(train, label=train_labels)
    xg_model = xgb.train(params, xg_train)
    # Create DMatrix required by XGBoost, test model
    xg_test = xgb.DMatrix(test, label=test_labels)
    # xg_test.feature_names = xg_train.feature_names  # Required because SMOTE apparently overwrites feature names
    xg_predict = xg_model.predict(xg_test)
    print(confusion_matrix(test_labels, xg_predict))
    prec = precision_score(test_labels, xg_predict)
    acc = accuracy_score(test_labels, xg_predict)
    recall = recall_score(test_labels, xg_predict)
    f = f1_score(test_labels, xg_predict)
    auc = roc_auc_score(test_labels, xg_predict)
    conf = confusion_matrix(test_labels, xg_predict)
    return prec, acc, recall, auc, f, conf

# Get the data
csv_data = get_data()

# Make K folds
K = 10
kf = KFold(K, shuffle=True)

# Set up results
xg_prec = 0
xg_acc = 0
xg_recall = 0
xg_auc = 0
xg_f = 0
xg_conf = 0

for fold, (train_index, test_index) in enumerate(kf.split(csv_data), 1):
    print("Starting interation %d" % fold)
    # Get train and test data
    train, test = csv_data.iloc[train_index], csv_data.iloc[test_index]

    # Extract labels, remove label from original data
    train_labels = train["label"]
    train = train.drop("label", axis=1)
    test_labels = test["label"]
    test = test.drop("label", axis=1)

    prec, acc, recall, auc, f, conf = xgboost(train, train_labels, test, test_labels)
    xg_prec += prec/K
    xg_acc += acc/K
    xg_recall += recall/K
    xg_auc += auc/K
    xg_f += f/K
    xg_conf += conf
