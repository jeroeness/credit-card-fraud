import sklearn
from sklearn.metrics import confusion_matrix
import pandas
from sklearn.model_selection import KFold
from sklearn.metrics import precision_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import recall_score
from sklearn.metrics import roc_auc_score
from sklearn.tree import DecisionTreeClassifier
import os
from sklearn.model_selection import cross_val_score
from sklearn import tree
from imblearn.over_sampling import SMOTE
from sklearn.datasets import make_classification
from sklearn.metrics import f1_score, confusion_matrix


def get_data():
    return pandas.read_csv("data/processed.csv")


def split_test_train(data, train_size):
    return sklearn.model_selection.train_test_split(data, train_size=train_size)


# Get the data
csv_data = get_data()
csv_data = csv_data.sample(frac=1)
target = csv_data.label
unlabeled_data = csv_data.drop("label", axis=1)

# SMOTE
sm = SMOTE(random_state=42)
xres, yres = sm.fit_resample(unlabeled_data, target)


# Classify
clf = DecisionTreeClassifier(random_state=0, max_depth=6)
cvs = cross_val_score(clf, xres, yres, cv=10)
print(cvs)

kf = KFold(n_splits=10)

for fold, (train_index, test_index) in enumerate(kf.split(target), 1):
    X_train = unlabeled_data.iloc[train_index]
    y_train = target.iloc[train_index]  # Based on your code, you might need a ravel call here, but I would look into how you're generating your y
    X_test = unlabeled_data.iloc[test_index]
    y_test = target.iloc[test_index]  # See comment on ravel and  y_train
    sm = SMOTE()
    X_train_oversampled, y_train_oversampled = sm.fit_sample(X_train, y_train)
    
    clf.fit(X_train_oversampled, y_train_oversampled)  
    y_pred = clf.predict(X_test)
    print('For fold {}:'.format(fold))
    print('Accuracy: {}'.format(clf.score(X_test, y_test)))
    print('f-score: {}'.format(f1_score(y_test, y_pred)))
    print('confusion mat: \n{}'.format(confusion_matrix(y_test, y_pred)))

clf.fit(xres, yres)
tree.export_graphviz(clf, out_file="data/tree.dot", feature_names=unlabeled_data.columns.tolist(), label="all", filled=True)
os.system("dot -Tpng data/tree.dot -o data/tree.png  ")