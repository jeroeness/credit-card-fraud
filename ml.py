import matplotlib.pyplot as plt
import pandas
import sklearn
import xgboost as xgb
from sklearn.metrics import auc
from sklearn.metrics import roc_curve
from sklearn.model_selection import KFold
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neural_network import MLPClassifier
from imblearn.over_sampling import SMOTE


def get_data():
    return pandas.read_csv("data/processed.csv")


def split_test_train(data, train_size):
    return sklearn.model_selection.train_test_split(data, train_size=train_size)


def xgboost(train, train_labels, test, test_labels):
    # XGBoost setup
    weight = (train_labels.size - train_labels.sum()) / train_labels.sum()
    params = {"objective": "binary:logistic",
              "eval_metric": "auc",
              "scale_pos_weight": weight}
    # Create DMatrix required by XGBoost, train model
    xg_train = xgb.DMatrix(train, label=train_labels)
    xg_model = xgb.train(params, xg_train)
    # Create DMatrix required by XGBoost, test model
    xg_test = xgb.DMatrix(test, label=test_labels)
    xg_test.feature_names = xg_train.feature_names  # Required because SMOTE apparently overwrites feature names
    xg_predict = xg_model.predict(xg_test)
    fpr, tpr, _ = roc_curve(test_labels, xg_predict)
    roc_auc = auc(fpr, tpr)
    return fpr, tpr, roc_auc


def knn(train, train_labels, test, test_labels):
    model = KNeighborsClassifier(n_neighbors=10)
    model.fit(train, train_labels)
    model_predict = model.predict_proba(test)
    fpr, tpr, _ = roc_curve(test_labels, model_predict[:, 1])
    roc_auc = auc(fpr, tpr)
    return fpr, tpr, roc_auc


def naive(train, train_labels, test, test_labels):
    model = GaussianNB()
    model.fit(train, train_labels)
    model_predict = model.predict_proba(test)
    fpr, tpr, _ = roc_curve(test_labels, model_predict[:, 1])
    roc_auc = auc(fpr, tpr)
    return fpr, tpr, roc_auc


def nnet(train, train_labels, test, test_labels):
    model = MLPClassifier(alpha=1)
    model.fit(train, train_labels)
    model_predict = model.predict_proba(test)
    fpr, tpr, _ = roc_curve(test_labels, model_predict[:, 1])
    roc_auc = auc(fpr, tpr)
    return fpr, tpr, roc_auc


# Get the data
csv_data = get_data()

# Make K folds
K = 10
kf = KFold(K, shuffle=True)
fold_number = 0

# Select ML algorithms
methods = ["XGBoost", "KNN", "Naive Bayes", "Neural Network"]
# methods = ["XGBoost"]
method_functions = {"XGBoost": xgboost,
                    "KNN": knn,
                    "Naive Bayes": naive,
                    "Neural Network": nnet}

# Set up result dicts
results = dict()
for method in methods:
    results[method] = dict()
    results[method]["fpr"] = dict()
    results[method]["tpr"] = dict()
    results[method]["roc_auc"] = dict()
    results[method]["smote-fpr"] = dict()
    results[method]["smote-tpr"] = dict()
    results[method]["smote-roc_auc"] = dict()

for train_index, test_index in kf.split(csv_data):
    print("Starting interation %d" % fold_number)
    # Get train and test data
    train, test = csv_data.iloc[train_index], csv_data.iloc[test_index]

    # Extract labels, remove label from original data
    train_labels = train["label"]
    train = train.drop("label", axis=1)
    test_labels = test["label"]
    test = test.drop("label", axis=1)

    sm = SMOTE(random_state=42)
    SMOTE_train, SMOTE_train_labels = sm.fit_resample(train, train_labels)

    # Call models
    for method in methods:
        results[method]["fpr"][fold_number], results[method]["tpr"][fold_number], results[method]["roc_auc"][
            fold_number] = method_functions[method](train, train_labels, test, test_labels)

    # Call models with SMOTEd data
    for method in methods:
        results[method]["smote-fpr"][fold_number], results[method]["smote-tpr"][fold_number], \
        results[method]["smote-roc_auc"][
            fold_number] = method_functions[method](SMOTE_train, SMOTE_train_labels, test, test_labels)

    # Increase counter
    fold_number += 1

for i, method in enumerate(methods):
    plt.figure(i * 2)
    for j in range(K):
        plt.plot(results[method]["fpr"][j],
                 results[method]["tpr"][j],
                 label="Fold {0}, auc={1:0.2f}".format(j, results[method]["roc_auc"][j]))
    plt.title("ROC for K=10 folds using {0}".format(method))
    plt.plot([0, 1], [0, 1], 'k--')
    plt.xlabel("False Positive Rate")
    plt.ylabel("True Positive Rate")
    plt.legend(loc="lower right")
    plt.show()

    plt.figure(i * 2 + 1)
    for j in range(K):
        plt.plot(results[method]["smote-fpr"][j],
                 results[method]["smote-tpr"][j],
                 label="Fold {0}, auc={1:0.2f}".format(j, results[method]["smote-roc_auc"][j]))
    plt.title("ROC for K=10 folds using {0} using SMOTE oversampling".format(method))
    plt.plot([0, 1], [0, 1], 'k--')
    plt.xlabel("False Positive Rate")
    plt.ylabel("True Positive Rate")
    plt.legend(loc="lower right")
    plt.show()
